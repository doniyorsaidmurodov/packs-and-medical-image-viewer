import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudyService {

  constructor(private http: HttpClient) {
  }

  getStudies(): Observable<any> {
    return this.http.get('https://packs.polito.uz/api/v1/dicom/patients/');
  }

  getStudyById(id: string): Observable<any> {
    return this.http.get(`https://packs.polito.uz/api/v1/dicom/patients/${id}/`);
  }

  saveData(data): Observable<any> {
    return this.http.post(`https://packs.polito.uz/api/v1/dicom/set-draw-data/`, data);
  }

  getData(): Observable<any> {
    return this.http.get('https://packs.polito.uz/api/v1/dicom/draw-data/');
  }

  addPatient(patient): Observable<any> {
    return this.http.post(`https://packs.polito.uz/api/v1/dicom/patients/`, patient)
  }


}
