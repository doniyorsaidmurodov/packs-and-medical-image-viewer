import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {StudyListComponent} from './study-list/study-list.component';
import {AuthComponent} from './auth/auth.component';
import {AuthGuard} from './shared/services/auth.guard';
import {PatientComponent} from './patient/patient.component';
import {RegistrationComponent} from './registrator/registration.component';
import {RadiologyComponent} from './radiolog/radiology.component';

const routes: Routes = [
  {path: '', component: StudyListComponent, canActivate: [AuthGuard]},
  {path: 'auth', component: AuthComponent},
  {path: 'patient/:id', component: PatientComponent, canActivate: [AuthGuard]},
  {path: 'registry', component: RegistrationComponent},
  {path: 'radiology', component: RadiologyComponent},
  {path: '**', redirectTo: '/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
